FROM centos:7
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;
#install openssh-server, sudo and passwd
RUN yum -y update; yum clean all
RUN yum -y install openssh-server openssh-clients passwd sudo; yum clean all
#can start sshd as a service so 
RUN systemctl enable sshd.service
#create an user name user and allowed sudo permission
ADD ./start.sh /start.sh
#create variable: AUTHORIZED_KEY for allowed ssh connection 
ADD ./run.sh /run.sh
VOLUME [ "/sys/fs/cgroup"]
#if AUTHORIZED_KEY is define the next cmd run
RUN mkdir /var/run/sshd
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
#RUN mkdir /etc/.ssh && touch /etc/.ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config && sed -ri 's/#UsePAM no/UsePAM no/g' /etc/ssh/sshd_config
RUN chmod 755 /start.sh && chmod 755 /run.sh
RUN ./start.sh
ENV AUTHORIZED_KEYS=AAAAB3NzaC1yc2EAAAADAQABAAABAQCoH3QKirrST2+tMHpCji8GeNTYPhX5Y2KTWYoRQIElcpT12oXtnoG6RdbTyjxIrSgtMOV9c8oZbhg95tgWeUWpLC0znMNHQxZ7yyqxQK+6RM19svfAIRuJ6FAMpXUlhkNSv30/gs7DeVxiFr7abbIrMpCa3BegrJ/FYdMzYk++mPM74le9jdaB6Q/pG71c3mMjCytBYy9+ZkH4u/qp6g7UcfYX/e9mpiMUdkaMNgaYky84ErTfPChWao79vTMZ9MKsNPQfqQhH9gHZebD+dUbl+fg0ANwfpmZltU6I7LFV6a7NXbwXZCRHcwTES8EIncbYkemfVIdrbF674YNu4Rbp
EXPOSE 22 8080
CMD  ["/run.sh"]
FROM centos
#mise a jour system
RUN yum update -y && \
yum install -y wget && \
yum install -y sudo && \
#installation du openssh-server et clients
yum -y install openssh-server openssh-clients 
#creation de l'utilisateur jenkins et l'ajouter au sudoers
RUN useradd jenkins && \
echo "jenkins" | passwd --stdin jenkins 
RUN echo "jenkins (ALL=ALL) ALL" >> /etc/sudoers
#generation de la clée ssh 
RUN mkdir /home/jenkins/.ssh
RUN ssh-keygen -t rsa -P adamounissi -f /home/jenkins/.ssh/id_rsa
#installation de java
RUN yum install -y java-1.8.0-openjdk && \
yum install -y java-1.8.0-openjdk-devel
ENV JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.181-3.b13.el7_5.x86_64
ENV JAVA_JRE=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.181-3.b13.el7_5.x86_64/jre
ENV PATH=$PATH:/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.181-3.b13.el7_5.x86_64/bin
#installation de maven
RUN mkdir /apps
ADD http://wwwftp.ciril.fr/pub/apache/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz /apps
# decompresser maven
RUN tar -xzf /apps/apache-maven-3.5.4-bin.tar.gz -C /apps
RUN  alternatives --install /usr/bin/mvn mvn /apps/apache-maven-3.5.4/bin/mvn 2000
ENV M2_HOME=/apps/apache-maven-3.5.4
ENV PATH=$PATH:/apps/apache-maven-3.5.4-bin/bin
#installation de git 
RUN yum install -y git
# installation d'ansible
RUN yum install -y epel-release && \
yum install ansible -y
#creation du repertoir ou on va mettre le jenkins.war
RUN mkdir -p /apps/jenkins
ADD https://updates.jenkins-ci.org/download/war/2.141/jenkins.war /apps/jenkins 
RUN chown -R jenkins:jenkins /apps/jenkins
CMD java -jar /apps/jenkins/jenkins.war 
VOLUME /apps/volume
EXPOSE 22 8080
